﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWayRegistrationService.WebAPI.Models
{
    public class RequestModel
    {

        public DateTime SendedOn { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public string MobilePhone { get; set; }

        public string Commentary { get; set; }

        public string ProgrammingExperience { get; set; }

        public bool OpenDay { get; set; }

        public string University { get; set; }

        public string Faculty { get; set; }

        public string Department { get; set; }

        public int YearOfAdmission { get; set; }

        public string EnglishLevel { get; set; }

        public string WorkExperience { get; set; }

        public string InformationSource { get; set; }

        public List<string> Subjects { get; set; } = new List<string>();
    }
}
