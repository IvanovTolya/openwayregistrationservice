﻿using Microsoft.Extensions.DependencyInjection;

using OpenWayRegistrationService.DAL.IManagers;
using OpenWayRegistrationService.DAL.Managers;

namespace OpenWayRegistrationService.WebAPI.Config
{
    public static class DependeciesConfig
    {
        //Создание инстансов для DI
        public static void AddDataServices(this IServiceCollection services)
        {
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IRequestManager, RequestManager>();
        }
    }
}
