﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using OpenWayRegistrationService.DAL;

namespace OpenWayRegistrationService.WebAPI.Config
{
    public static class DatabaseConfig
    {

        public static void AddDataContext(this IServiceCollection services, string connection)
        {
            services.AddDbContext<DataContext>(conf => conf.UseSqlServer(connection));
        }
    }
}
