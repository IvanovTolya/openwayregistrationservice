﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace OpenWayRegistrationService.WebAPI.Config
{
    // Настройки авторизации
    // Валидируется только ключ
    public static class AuthOptions
    {
        private const string KEY = "~Y=`^vHDqcX6j5,3";

        public static DateTime LIFETIME = DateTime.Now.AddHours(24); //срок жизни токена в секундах (сутки)

        public static SymmetricSecurityKey SymmetricSecurityKey
        {
            get
            {
                return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
            }
        }

        public static void AddAuth(this IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = SymmetricSecurityKey
                    };
                });
        }
    }
}
