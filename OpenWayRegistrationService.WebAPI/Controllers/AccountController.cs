﻿using System;
using System.Net;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

using OpenWayRegistrationService.WebAPI.Config;
using OpenWayRegistrationService.WebAPI.Models;
using OpenWayRegistrationService.DAL.IManagers;

namespace OpenWayRegistrationService.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AccountController : Controller
    {
        // Занесение пользователей в БД осуществляется руками
        IUserManager UserManager { get; set; }

        public AccountController(IUserManager userManager)
        {
            UserManager = userManager;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]AccountModel account)
        {
            var user = await UserManager.GetUsers().Where(x => x.Login == account.Login).FirstOrDefaultAsync();

            if (user == null || user.Password != account.Password)
            {
                return Unauthorized();
            }
            //Создаем токин, подшиваем системную фразу и возвращаем залогиненому пользователю
            var token = new JwtSecurityToken(
                claims: new[] { new Claim(ClaimTypes.Name, user.Login) },
                expires: AuthOptions.LIFETIME,
                signingCredentials: new SigningCredentials(
                    AuthOptions.SymmetricSecurityKey, SecurityAlgorithms.HmacSha256
                )
            );

            string authorization = "Bearer " + new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(authorization);
        }
    }
}