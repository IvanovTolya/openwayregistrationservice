﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using OpenWayRegistrationService.DAL.Domains;
using OpenWayRegistrationService.DAL.IManagers;
using OpenWayRegistrationService.WebAPI.Models;

namespace OpenWayRegistrationService.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class RequestsController : Controller
    {
        IRequestManager RequestManager { get; set; }

        public RequestsController(IRequestManager requestManager)
        {
            RequestManager = requestManager;
        }

        [HttpPost]
        public async Task<IActionResult> Check()
        {
            return Ok("yes");
        }

        [AllowAnonymous]
        [HttpPost("new")]
        public async Task<IActionResult> NewRequest([FromBody]RequestModel model)
        {
            // Я не успеваю допилить форму на фронте, дак тчо часть данных будет в хардкоде(
            // Очень классная валидация :)
            if (model.FirstName == null || model.LastName == null || model.Birthday == null ||
                model.ProgrammingExperience == null || model.University == null || model.Faculty == null ||
                model.Department == null || model.EnglishLevel == null || model.Email == null)
                return BadRequest("Не заполнены обязательные поля!");

            var dbModel = new Request
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                EnglishLevel = model.EnglishLevel,
                Department = model.Department,
                Birthday = model.Birthday,
                Commentary = model.Commentary,
                Faculty = model.Faculty,
                InformationSource = model.InformationSource,
                MobilePhone = model.MobilePhone,
                OpenDay = model.OpenDay,
                ProgrammingExperience = model.ProgrammingExperience,
                University = model.University,
                WorkExperience = model.WorkExperience,
                YearOfAdmission = model.YearOfAdmission,
                Email = model.Email
            };

            // Немножко костыльное заполнение интересов, нужен автомаппер
            foreach (var subject in model.Subjects)
            {
                dbModel.Subjects.Add(new Request_Subject
                {
                    Request = dbModel,
                    Subject = subject
                });
            }

            await RequestManager.NewRequest(dbModel);

            return Ok("Added");
        }

        [HttpPost("all")]
        public async Task<IActionResult> AllRequests()
        {
            var requests = await RequestManager.GetRequests().ToListAsync();
            var response = requests.Select(x => new RequestModel
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
                EnglishLevel = x.EnglishLevel,
                Department = x.Department,
                Birthday = x.Birthday,
                Commentary = x.Commentary,
                Faculty = x.Faculty,
                InformationSource = x.InformationSource,
                MobilePhone = x.MobilePhone,
                OpenDay = x.OpenDay,
                ProgrammingExperience = x.ProgrammingExperience,
                University = x.University,
                WorkExperience = x.WorkExperience,
                YearOfAdmission = x.YearOfAdmission,
                Email = x.Email,
                Subjects = x.Subjects.Select(s => s.Subject).ToList(), //тоже костыльно
                SendedOn = x.SendedOn
            }).ToList();

            return Ok(response);
        }

    }
}