export class RequestForm{

    public FirstName: string;

    public SendedOn: Date;

    public LastName: string;

    public Email: string;

    public Birthday: Date;

    public MobilePhone: string;

    public Commentary: string;

    public ProgrammingExperience: string;

    public OpenDay: boolean;

    public University: string;

    public Faculty: string;

    public Department: string;

    public YearOfAdmission: number;

    public EnglishLevel: string;

    public WorkExperience: string;

    public InformationSource: string;

    public Subjects: string[];
}