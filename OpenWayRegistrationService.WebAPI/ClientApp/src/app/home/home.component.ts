import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Location } from '@angular/common';
import { RequestForm } from './requestform'
import { Subjects } from './subjectsenum';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private http: HttpClient, private location: Location) { }

  request: RequestForm;
  subjects = Subjects;
  subjectsMatrix: boolean[];

  ngOnInit() {
    this.request = new RequestForm();
    this.subjectsMatrix = [];
  }
  //
  //TODO: убрать этот костыль
  rebuildSubjects(){
    this.request.Subjects = [];
    for(let i = 0; i <= this.subjectsMatrix.length; i++){
      if(this.subjectsMatrix[i]){
        this.request.Subjects.push(this.subjects[i]);
      }
    }
  }

  //TODO: вынести в сервисы
  sendRequest(){
    this.request.ProgrammingExperience = "God of Web";
    this.request.University = "ITMO";
    this.request.Faculty = "ITIP";
    this.request.Department = "IS";
    this.request.EnglishLevel = "Very High";

    return this.http.post('api/requests/new', this.request).subscribe(data => {
      alert("Заявка отправлена!");
      location.reload();
    }); 
  }

}

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    const keys = Object.keys(data);
    return keys.slice(keys.length / 2);
  }
}
