export class RequestForm{

    public firstName: string;

    public sendedOn: Date;

    public lastName: string;

    public email: string;

    public birthday: Date;

    public mobilePhone: string;

    public commentary: string;

    public programmingExperience: string;

    public openDay: boolean;

    public university: string;

    public faculty: string;

    public Department: string;

    public yearOfAdmission: number;

    public englishLevel: string;

    public workExperience: string;

    public informationSource: string;

    public subjects: string[];
}