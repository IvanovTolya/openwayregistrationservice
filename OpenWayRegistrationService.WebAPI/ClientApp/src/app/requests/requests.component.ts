import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestForm } from './requestform'


@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RequestsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { }

  requests: RequestForm[];

  headerWithToken: string;

  ngOnInit() {
    // this.headerWithToken = this.route.snapshot.paramMap.get('headerWithToken');
    this.headerWithToken = localStorage.getItem("token");
    if (this.headerWithToken == null) {
      this.router.navigate(['/']);
    }

    this.getRequests().subscribe(
      data => {
        this.requests = data;
      });
  }

  getRequests() {
    return this.http.post<RequestForm[]>('api/requests/all', {}, 
    {
      headers: new HttpHeaders().set('Authorization', this.headerWithToken)
    });
  }
}
