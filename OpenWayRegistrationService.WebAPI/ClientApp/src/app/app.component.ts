import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public counter: number = 0;
  logoPath: string = "";

  public returnCount(){
    return this.counter;
  }
}
