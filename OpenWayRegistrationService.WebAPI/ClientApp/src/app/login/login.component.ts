import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { AccountFormData } from './account';
import { HttpClient } from '@angular/common/http';
import { AlertComponent } from 'bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  account: AccountFormData;

  alert: any;

  ngOnInit() {
    this.account = new AccountFormData();
  }

  sendRequest(){

    return this.http.post<string>('api/account/login', this.account).subscribe(
      data => {
      localStorage.setItem('token', data);
      this.router.navigate(['../requests']);
    },
    error =>
    {
      alert("Неудачная попытка входа");
    }
  ); 
  }

}
