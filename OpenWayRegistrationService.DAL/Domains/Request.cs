﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWayRegistrationService.DAL.Domains
{
    public class Request
    {
        public long Id { get; set; }

        public DateTime SendedOn { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public string MobilePhone { get; set; }

        public string Commentary { get; set; }

        public string ProgrammingExperience { get; set; }

        public bool OpenDay { get; set; }

        public string University { get; set; }

        public string Faculty { get; set; }

        public string Department { get; set; }

        public int YearOfAdmission { get; set; }

        public string EnglishLevel { get; set; }

        public string WorkExperience { get; set; }

        public string InformationSource { get; set; }

        public List<Request_Subject> Subjects { get; set; } = new List<Request_Subject>(); 
    }
}
