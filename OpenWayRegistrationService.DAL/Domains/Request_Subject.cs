﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWayRegistrationService.DAL.Domains
{
    public class Request_Subject
    {
        public long Id { get; set; }

        public long RequestId { get; set; }

        public Request Request { get; set; }

        public string Subject { get; set; }
    }
}
