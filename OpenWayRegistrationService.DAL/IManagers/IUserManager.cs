﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using OpenWayRegistrationService.DAL.Domains;

namespace OpenWayRegistrationService.DAL.IManagers
{
    public interface IUserManager
    {
        /// <summary>
        /// Получение списка всех пользователей
        /// </summary>
        /// <returns>Список пользователей</returns>
        IQueryable<User> GetUsers();
    }
}
