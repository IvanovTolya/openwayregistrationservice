﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenWayRegistrationService.DAL.Domains;

namespace OpenWayRegistrationService.DAL.IManagers
{
    public interface IRequestManager
    {

        /// <summary>
        /// Создание реквеста
        /// </summary>
        /// <returns>Реквест</returns>
        Task<Request> NewRequest(Request request);

        /// <summary>
        /// Получить все реквесты
        /// </summary>
        /// <returns>Реквесты</returns>
        IQueryable<Request> GetRequests();
    }
}
