﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;

using OpenWayRegistrationService.DAL.Domains;

namespace OpenWayRegistrationService.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        
        public DbSet<Request_Subject> Request_Subjects { get; set; }

        public DbSet<Request> Requests { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
