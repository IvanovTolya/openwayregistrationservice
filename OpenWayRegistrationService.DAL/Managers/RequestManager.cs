﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OpenWayRegistrationService.DAL.Domains;
using OpenWayRegistrationService.DAL.IManagers;

namespace OpenWayRegistrationService.DAL.Managers
{
    public class RequestManager : IRequestManager
    {
        private readonly DataContext dataContext;

        public RequestManager(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<Request> NewRequest(Request request)
        {
            request.SendedOn = DateTime.Now;
            await dataContext.AddAsync(request);
            await dataContext.SaveChangesAsync();


            return request;
        }

        public IQueryable<Request> GetRequests()
        {
            return dataContext.Requests.Include(x => x.Subjects);
        }
    }
}
