﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenWayRegistrationService.DAL.Domains;
using OpenWayRegistrationService.DAL.IManagers;

namespace OpenWayRegistrationService.DAL.Managers
{
    public class UserManager : IUserManager
    {
        private readonly DataContext dataContext;

        public UserManager(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IQueryable<User> GetUsers()
        {
            return dataContext.Users;
        }
    }
}
